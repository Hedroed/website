---
title: "How did I setup this blog ?"
date: 2020-06-27T13:51:55+02:00
draft: false
toc: false
images:
tags:
  - install
  - hugo
  - gitlab
---

### Setting up a blog with Hugo and host it on Gitlab

This Blog is hosted for free by Gitlab on a Gitlab Page and builded automatically by Gitlab CI when I commit changes.

It is builded with [Hugo](https://gohugo.io/), a simple framework for building blogs.

Here is a little tutorial to recreate it.

## 1. Find a cool theme :sunglasses:

On this page https://themes.gohugo.io/ you can find a list of community themes compatible with Hugo.

For my blog I choose `hello-friend-ng`.

Once you found your theme, on the theme description find the git url or a git command looking as:
```
git submodule add https://github.com/rhazdon/hugo-theme-hello-friend-ng.git themes/hello-friend-ng
```

You can also found configuration options for the theme.


## 2. Install and setup a default Hugo blog in local

Install instruction can be found on https://gohugo.io/getting-started/installing/.

Official instruction can be found on https://gohugo.io/getting-started/quick-start/.

Use the theme you choose before.

If it exists, you can take an example of how to configure the demo site for your theme on its git repository.

## 3. Create Gitlab CI and Page

You can find instruction to host your blog on Gitlab here: https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/.

*There are documentation for Github and other services but I found Gitlab setup easier.*

## 4. Connect with a custom domain name

**:warning: For this section you need a personal domain name and to know how to edit a DNS zone.**

At this moment you must have your blog online but the URL must be something like `https://<YourUsername>.gitlab.io/<your-hugo-site>/`.

If you want to bind a custom domain in Gitlab repository go to **Settings → Pages → New Domain**.

Enter your domain. Now you need to verify and to connect your domain to Gitlab.

First add a `A` entry to your DNS Zone pointing to Gitlab IP: `35.185.44.232`.

Secondly add a `TXT` entry containing value describe in **Verification status** section.

Wait a couple of minutes for DNS cache update and click **Retry verification icon**.

Save wait another couple of minutes for Gitlab to create SSL certificate for your website.

After that you can connect to it with your custom domain name.
