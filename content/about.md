---
title: About
date: 2023-11-13
---

**Hello !**

I'm Nathan, I'm a IT cybersecurity engineer at Synacktiv. 🥷

I like playing and organizing CTF. You can find me on Root-me and HackTheBox.

I also enjoy playing with Raspberry and IOT. 💡

