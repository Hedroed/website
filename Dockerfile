FROM alpine:3.13 AS build

# The Hugo version
ARG VERSION=0.82.0

ADD https://github.com/gohugoio/hugo/releases/download/v${VERSION}/hugo_${VERSION}_Linux-64bit.tar.gz /hugo.tar.gz
RUN tar -zxvf hugo.tar.gz
RUN /hugo version

WORKDIR /site
COPY . .

# And then we just run Hugo
RUN /hugo --minify 

# stage 2
FROM nginx:1.19-alpine

WORKDIR /usr/share/nginx/html/

# Clean the default public folder
RUN rm -rf * .??*

COPY --from=build /site/public /usr/share/nginx/html
